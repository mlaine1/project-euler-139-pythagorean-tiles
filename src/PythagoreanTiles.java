import components.queue.Queue;
import components.queue.Queue2;
import components.simplereader.SimpleReader;
import components.simplereader.SimpleReader1L;
import components.simplewriter.SimpleWriter;
import components.simplewriter.SimpleWriter1L;
import components.stopwatch.Stopwatch;
import components.stopwatch.Stopwatch1;

public final class PythagoreanTiles {

    private PythagoreanTiles() {
    }

    public static void main(String[] args) {
        SimpleReader in = new SimpleReader1L();
        SimpleWriter outputToConsole = new SimpleWriter1L();
        SimpleWriter outputAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outputEndInformation = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");

        long userLimit = 0;
        int tripleCount = 0;
        long[] rootTriple = { 3, 4, 5 };
        Stopwatch runTime = new Stopwatch1();

        outputToConsole.print("Enter a perimeter limit: ");
        userLimit = in.nextLong();

        runTime.start();

        tripleCount = generatePythagoreanTriples(userLimit, rootTriple,
                outputToConsole, outputAllTriangles, outputEndInformation);

        runTime.stop();

        outputEndProgramMessages(outputEndInformation, outputToConsole,
                userLimit, tripleCount, runTime.elapsed());

        in.close();
        outputToConsole.close();
        outputAllTriangles.close();
        outputEndInformation.close();
    }

    protected static int generatePythagoreanTriples(long perimeterLimit,
            long[] root, SimpleWriter outConsole, SimpleWriter outAllTriangles,
            SimpleWriter outFillingTriangles) {

        int count = 0;
        long totalTriangles = 0;
        Queue<long[]> rootTriples = new Queue2<>();
        rootTriples.enqueue(root);

        while (rootTriples.length() > 0) {
            long[] tempRoot = rootTriples.dequeue();

            final long[][] matrixMultipler1 = { { -1, 2, 2 }, { -2, 1, 2 },
                    { -2, 2, 3 } };
            final long[][] matrixMultipler2 = { { 1, 2, 2 }, { 2, 1, 2 },
                    { 2, 2, 3 } };
            final long[][] matrixMultipler3 = { { 1, -2, 2 }, { 2, -1, 2 },
                    { 2, -2, 3 } };

            long[] child1 = multiplySquareMatrixAndVector(matrixMultipler1,
                    tempRoot);
            long[] child2 = multiplySquareMatrixAndVector(matrixMultipler2,
                    tempRoot);
            long[] child3 = multiplySquareMatrixAndVector(matrixMultipler3,
                    tempRoot);

            if (allowedPerimeter(perimeterLimit, child1)) {
                rootTriples.enqueue(child1);
            }
            if (allowedPerimeter(perimeterLimit, child2)) {
                rootTriples.enqueue(child2);
            }
            if (allowedPerimeter(perimeterLimit, child3)) {
                rootTriples.enqueue(child3);
            }
            if (allowedPerimeter(perimeterLimit, tempRoot)) {
                if (triangleCanTileSquare(tempRoot)) {
                    addTriangleToFile(tempRoot, outFillingTriangles);
                    count++;
                }
                totalTriangles++;
            }
            outAllTriangles.print("Count: " + totalTriangles + "\t\t\t");
            addTriangleToFile(tempRoot, outAllTriangles);
            updateConsole(totalTriangles, rootTriples.length(), count,
                    outConsole);
        }
        return count;
    }

    protected static boolean allowedPerimeter(long perimeterMax, long[] sides) {
        long perimeter = 0;
        for (int i = 0; i < sides.length; i++) {
            perimeter += sides[i];
        }
        return perimeter < perimeterMax;
    }

    protected static long[] multiplySquareMatrixAndVector(long[][] squareMatrix,
            long[] vector) {
        long[] productVector = new long[vector.length];

        for (int i = 0; i < squareMatrix.length; i++) {
            for (int k = 0; k < squareMatrix[0].length; k++) {
                productVector[i] += squareMatrix[i][k] * vector[k];
            }
        }
        return productVector;
    }

    protected static boolean triangleCanTileSquare(long[] triangle) {
        boolean canFillSquare = false;

        if (triangle.length > 0) {
            long[] sidesInOrder = getSidesAscendingOrder(triangle);
            long aSide = sidesInOrder[0];
            long bSide = sidesInOrder[1];
            long cSide = sidesInOrder[2];

            long smallSquareArea = ((bSide - aSide) * (bSide - aSide));
            long largeSquareArea = (cSide * cSide);
            canFillSquare = largeSquareArea % smallSquareArea == 0;
        }
        return canFillSquare;
    }

    protected static long[] getSidesAscendingOrder(long[] triangle) {
        long aSide = 0;
        long bSide = 0;
        long cSide = 0;
        long side1 = triangle[0];
        long side2 = triangle[1];
        long side3 = triangle[2];
        long[] inOrder = new long[3];

        if (side1 > side2) {
            if (side2 > side3) {
                aSide = side3;
                bSide = side2;
                cSide = side1;
            } else if (side3 > side1) {
                aSide = side2;
                bSide = side1;
                cSide = side3;
            } else {
                aSide = side2;
                bSide = side3;
                cSide = side1;
            }
        } else {
            if (side3 > side2) {
                aSide = side1;
                bSide = side2;
                cSide = side3;
            } else if (side3 > side1) {
                aSide = side1;
                bSide = side3;
                cSide = side2;
            } else {
                aSide = side3;
                bSide = side1;
                cSide = side2;
            }
        }
        inOrder[0] = aSide;
        inOrder[1] = bSide;
        inOrder[2] = cSide;
        return inOrder;
    }

    protected static void addTriangleToFile(long[] triangle, SimpleWriter out) {
        for (int i = 0; i < triangle.length; i++) {
            out.print(triangle[i] + "\t\t\t");
        }
        out.println();
    }

    protected static void updateConsole(long totalTrianglesTested,
            long trianglesToTest, int fillingCount, SimpleWriter out) {
        out.print("Total Triangles Tested: " + totalTrianglesTested + "\t");
        out.print("Triangles to test: " + trianglesToTest);
        out.println("\n" + "Filling: " + fillingCount);
    }

    protected static void outputEndProgramMessages(
            SimpleWriter outFillTriangles, SimpleWriter outConsole,
            long perimeterLimit, int tripleCount, int runTime) {
        outConsole.println("Total runtime miliseconds: " + runTime);
        outFillTriangles.println("Total count: " + tripleCount);
        outConsole.println(
                "The number of Filling Pythagorean Triples with perimeter < "
                        + perimeterLimit + " is: " + tripleCount);
    }

}
