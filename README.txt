Project Euler 139: Pythagorean Tiles	      12/6/18

Problem Description
-----------------------------------------------------
Link: https://projecteuler.net/problem=139

For each unique right triangle with sides (a,b,c) and prerimeter < 100,000,000
check if it satisfies the condition:   (c^2)mod((b - a)^2) == 0
and output the total count of triangles that satisfy the condition 


Folder descriptions
-----------------------------------------------------
Project was made in eclipse

data folder contains two files: 
-All_Pythag_Triples.txt
	The total count of all triangles generated with their side lengths
-Filling_Pythat_Triples_Count.txt
	Each filling triangles with the total count

src folder contains the program file: 
-PythagoreanTiles.java 
	
	prompts user to enter a max perimeter
	
	the program file outputs to the console run time information:
		-total triangles tested
		-triangles generated left to test
		-the total count of filling triangles after program terminates

	The program file outputs to two files located in the data folder:
		-All_Pythag_Triples.txt
		-Filling_Pythat_Triples_Count.txt


test folder contains file with JUnit test cases: 
	-PythagoreanTilesTest.java
		Tests appropriate methods in PythagoreanTiles.java 


NOTE: both PythagoreanTiles.java and PythagoreanTilesTest.java use
imported components from OSU's API found at:
http://web.cse.ohio-state.edu/software/common/doc/


Program Usage
-----------------------------------------------------

Run PythagoreanTiles.java
-When prompted for perimeter limit enter: 100000000
-After program runs check console output for the count 10
-Open data folder and open Filling_Pythat_Triples_Count.txt
and compare count with console output
