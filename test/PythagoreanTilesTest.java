import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import components.simplewriter.SimpleWriter;
import components.simplewriter.SimpleWriter1L;

public class PythagoreanTilesTest {

    @Test
    public void testGeneratePythagoreanTriplesZeroLimit() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 0;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 0;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesLimitIsRootPerimeter() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 12;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 0;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesOnlyRootBelowPerim() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 13;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 1;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesOneChildBelowPerimOneFills() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 31;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 1;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesTwoChildrenBelowPerimOneFills() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 41;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 1;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesOneSubChildOfChildBelowPerimOneFill() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 57;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 1;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesTwoFills() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 71;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 2;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesOneMillionLimit() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 1000000;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 7;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesTenMillionLimit() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 10000000;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 8;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testGeneratePythagoreanTriplesHundredMillionLimit() {
        SimpleWriter outConsole = new SimpleWriter1L();
        SimpleWriter outAllTriangles = new SimpleWriter1L(
                "data/All_Pythag_Triples.txt");
        SimpleWriter outFillingTriangles = new SimpleWriter1L(
                "data/Filling_Pythat_Triples_Count.txt");
        long perimeterLimit = 100000000;
        long[] root = { 3, 4, 5 };
        long[] rootCopy = { 3, 4, 5 };
        int expectedOutput = 10;

        int actualOutput = PythagoreanTiles.generatePythagoreanTriples(
                perimeterLimit, root, outConsole, outAllTriangles,
                outFillingTriangles);

        boolean rootRestored = Arrays.equals(root, rootCopy);

        assertEquals(expectedOutput, actualOutput);
        assertTrue(rootRestored);
    }

    @Test
    public void testAllowedPerimeterZeroPerimeter() {
        long perimeterMax = 0;
        long[] triangle = { 3, 4, 5 };
        long[] triangleCopy = { 3, 4, 5 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(!allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterZeroTriangleZeroPerimeter() {
        long perimeterMax = 0;
        long[] triangle = { 0, 0, 0 };
        long[] triangleCopy = { 0, 0, 0 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(!allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterZeroTriangleNonZeroPerimeter() {
        long perimeterMax = 1;
        long[] triangle = { 0, 0, 0 };
        long[] triangleCopy = { 0, 0, 0 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterTrianglePerimeterUnderLimit() {
        long perimeterMax = 4;
        long[] triangle = { 1, 1, 1 };
        long[] triangleCopy = { 1, 1, 1 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterTrianglePerimeterUnderTwoDigitLimit() {
        long perimeterMax = 40;
        long[] triangle = { 35, 2, 2 };
        long[] triangleCopy = { 35, 2, 2 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterAllowedAboveIntCap() {
        long perimeterMax = 2147483650L;
        long[] triangle = { 2147483646L, 2, 1 };
        long[] triangleCopy = { 2147483646L, 2, 1 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testAllowedPerimeterNotAllowedAboveIntCap() {
        long perimeterMax = 2147483647L;
        long[] triangle = { 2147483646L, 2, 2 };
        long[] triangleCopy = { 2147483646L, 2, 2 };
        boolean allowed = PythagoreanTiles.allowedPerimeter(perimeterMax,
                triangle);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(!allowed);
        assertTrue(triangleRestored);
    }

    @Test
    public void testMultiplySquareMatrixAndVectorZeroVector() {
        long[][] squareMatrix = { { -1, 2, 2 }, { -2, 1, 2 }, { -2, 2, 3 } };
        long[][] squareCopy = { { -1, 2, 2 }, { -2, 1, 2 }, { -2, 2, 3 } };
        long[] vector = { 0, 0, 0 };
        long[] vectorCopy = { 0, 0, 0 };
        long[] expectedResult = { 0, 0, 0 };

        long[] actualResult = PythagoreanTiles
                .multiplySquareMatrixAndVector(squareMatrix, vector);

        boolean matrixRestored = Arrays.deepEquals(squareMatrix, squareCopy);
        boolean vectorRestored = Arrays.equals(vector, vectorCopy);
        boolean correctResult = Arrays.equals(expectedResult, actualResult);

        assertTrue(matrixRestored);
        assertTrue(vectorRestored);
        assertTrue(correctResult);
    }

    @Test
    public void testMultiplySquareMatrixAndVectorZeroMatrix() {
        long[][] squareMatrix = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        long[][] squareCopy = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };
        long[] vector = { 1, 1, 1 };
        long[] vectorCopy = { 1, 1, 1 };
        long[] expectedResult = { 0, 0, 0 };

        long[] actualResult = PythagoreanTiles
                .multiplySquareMatrixAndVector(squareMatrix, vector);

        boolean matrixRestored = Arrays.deepEquals(squareMatrix, squareCopy);
        boolean vectorRestored = Arrays.equals(vector, vectorCopy);
        boolean correctResult = Arrays.equals(expectedResult, actualResult);

        assertTrue(matrixRestored);
        assertTrue(vectorRestored);
        assertTrue(correctResult);
    }

    @Test
    public void testMultiplySquareMatrixAndVectorMatrix1NonZeroVector() {
        long[][] squareMatrix = { { -1, 2, 2 }, { -2, 1, 2 }, { -2, 2, 3 } };
        long[][] squareCopy = { { -1, 2, 2 }, { -2, 1, 2 }, { -2, 2, 3 } };
        long[] vector = { 5, 12, 13 };
        long[] vectorCopy = { 5, 12, 13 };
        long[] expectedResult = { 45, 28, 53 };

        long[] actualResult = PythagoreanTiles
                .multiplySquareMatrixAndVector(squareMatrix, vector);

        boolean matrixRestored = Arrays.deepEquals(squareMatrix, squareCopy);
        boolean vectorRestored = Arrays.equals(vector, vectorCopy);
        boolean correctResult = Arrays.equals(expectedResult, actualResult);

        assertTrue(matrixRestored);
        assertTrue(vectorRestored);
        assertTrue(correctResult);
    }

    @Test
    public void testMultiplySquareMatrixAndVectorMatrix2NonZeroVector() {
        long[][] squareMatrix = { { 1, 2, 2 }, { 2, 1, 2 }, { 2, 2, 3 } };
        long[][] squareCopy = { { 1, 2, 2 }, { 2, 1, 2 }, { 2, 2, 3 } };
        long[] vector = { 5, 12, 13 };
        long[] vectorCopy = { 5, 12, 13 };
        long[] expectedResult = { 55, 48, 73 };

        long[] actualResult = PythagoreanTiles
                .multiplySquareMatrixAndVector(squareMatrix, vector);

        boolean matrixRestored = Arrays.deepEquals(squareMatrix, squareCopy);
        boolean vectorRestored = Arrays.equals(vector, vectorCopy);
        boolean correctResult = Arrays.equals(expectedResult, actualResult);

        assertTrue(matrixRestored);
        assertTrue(vectorRestored);
        assertTrue(correctResult);
    }

    @Test
    public void testMultiplySquareMatrixAndVectorMatrix3NonZeroVector() {
        long[][] squareMatrix = { { 1, -2, 2 }, { 2, -1, 2 }, { 2, -2, 3 } };
        long[][] squareCopy = { { 1, -2, 2 }, { 2, -1, 2 }, { 2, -2, 3 } };
        long[] vector = { 5, 12, 13 };
        long[] vectorCopy = { 5, 12, 13 };

        long[] expectedResult = { 7, 24, 25 };

        long[] actualResult = PythagoreanTiles
                .multiplySquareMatrixAndVector(squareMatrix, vector);

        boolean matrixRestored = Arrays.deepEquals(squareMatrix, squareCopy);
        boolean vectorRestored = Arrays.equals(vector, vectorCopy);
        boolean correctResult = Arrays.equals(expectedResult, actualResult);

        assertTrue(matrixRestored);
        assertTrue(vectorRestored);
        assertTrue(correctResult);
    }

    @Test
    public void testTriangleCanTileSquareEmptyArray() {
        long[] triangle = {};
        boolean actualResult = PythagoreanTiles.triangleCanTileSquare(triangle);

        assertTrue(!actualResult);
    }

    @Test
    public void testTriangleCanTileSquareTriangleDoesFill() {
        long[] triangle = { 3, 4, 5 };
        boolean actualResult = PythagoreanTiles.triangleCanTileSquare(triangle);

        assertTrue(actualResult);
    }

    @Test
    public void testTriangleCanTileSquareTriangleCanNotFill() {
        long[] triangle = { 5, 12, 13 };
        boolean actualResult = PythagoreanTiles.triangleCanTileSquare(triangle);

        assertTrue(!actualResult);
    }

    @Test
    public void testTriangleCanTileSquare4DigitTriangleDoesFill() {
        long[] triangle = { 1680, 1260, 2100 };
        boolean actualResult = PythagoreanTiles.triangleCanTileSquare(triangle);

        assertTrue(actualResult);
    }

    @Test
    public void testGetSidesAscendingOrderInOrder() {
        long[] triangle = { 1, 2, 3 };
        long[] triangleCopy = { 1, 2, 3 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

    @Test
    public void testGetSidesAscendingOrderOutOfOrder1() {
        long[] triangle = { 1, 3, 2 };
        long[] triangleCopy = { 1, 3, 2 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

    @Test
    public void testGetSidesAscendingOrderOutOfOrder2() {
        long[] triangle = { 2, 1, 3 };
        long[] triangleCopy = { 2, 1, 3 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

    @Test
    public void testGetSidesAscendingOrderOutOfOrder3() {
        long[] triangle = { 2, 3, 1 };
        long[] triangleCopy = { 2, 3, 1 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

    @Test
    public void testGetSidesAscendingOrderOutOfOrder4() {
        long[] triangle = { 3, 1, 2 };
        long[] triangleCopy = { 3, 1, 2 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

    @Test
    public void testGetSidesAscendingOrderOutOfOrder5() {
        long[] triangle = { 3, 2, 1 };
        long[] triangleCopy = { 3, 2, 1 };
        long[] expectedResult = { 1, 2, 3 };
        long[] actualResult = PythagoreanTiles.getSidesAscendingOrder(triangle);

        boolean arraysEqual = Arrays.equals(expectedResult, actualResult);
        boolean triangleRestored = Arrays.equals(triangle, triangleCopy);

        assertTrue(arraysEqual);
        assertTrue(triangleRestored);
    }

}
